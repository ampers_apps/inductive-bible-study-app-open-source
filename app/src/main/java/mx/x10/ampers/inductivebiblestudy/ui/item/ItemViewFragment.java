package mx.x10.ampers.inductivebiblestudy.ui.item;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import mx.x10.ampers.inductivebiblestudy.BibleStudyDbHelper;
import mx.x10.ampers.inductivebiblestudy.MainActivity;
import mx.x10.ampers.inductivebiblestudy.R;
import mx.x10.ampers.inductivebiblestudy.ui.study.StudyFragment;

public class ItemViewFragment extends Fragment {

    long studyId;
    String itemString;
    String passageHeading;
    String itemContent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.fragment_item_view, container, false);

        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.item_view_title);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(androidx.appcompat.R.drawable.abc_ic_ab_back_material);
        ((MainActivity)getActivity()).lockDrawer();

        Bundle bundle = this.getArguments();

        studyId = bundle.getLong("studyId", -1);
        itemString = bundle.getString("item","");
        String passageCopyright = "";

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {

                goBack();

            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        if (studyId != -1) {

            // do sql based off study id and item
            BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getContext());
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            Cursor cursor = db.rawQuery("SELECT " + itemString + ",heading, copyright FROM study WHERE id=" + studyId, null);

            if (cursor.moveToFirst()) {
                itemContent = cursor.getString(0);
                passageHeading = cursor.getString(1);
                passageCopyright = cursor.getString(2);
            }

            cursor.close();
            db.close();

            if (itemContent == null) {

                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                String toolbarPref = pref.getString("pref_defaultToolbar", getString(R.string.pref_defaultToolbar_default));

                if (!toolbarPref.equals("none")) {
                    //Toast.makeText(getContext(), R.string.loading_editor, Toast.LENGTH_LONG).show();
                }

                //Goto edit screen right away
                Fragment editFragment = new ItemEditFragment();
                Bundle editBundle = new Bundle();
                editBundle.putLong("studyId", studyId);
                editBundle.putString("item", itemString);
                editFragment.setArguments(editBundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left)
                        .replace(R.id.nav_host_fragment, editFragment)
                        .addToBackStack("tag")
                        .commit();
            }
            else {

                String headingText = "";

                switch (itemString) {
                    case "passage":
                        headingText = passageHeading;
                        TextView itemCopyright = root.findViewById(R.id.itemCopyright);
                        itemCopyright.setText(Html.fromHtml("<small>" + passageCopyright + "</small>"));
                        itemCopyright.setVisibility(View.VISIBLE);

                        break;
                    case "observations":
                        headingText = getString(R.string.observations_heading);
                        break;
                    case "questions":
                        headingText = getString(R.string.questions_heading);
                        break;
                    case "key":
                        headingText = getString(R.string.key_heading);
                        break;
                    case "application":
                        headingText = getString(R.string.application_heading);
                        break;
                }

                TextView heading = root.findViewById(R.id.itemHeading);
                heading.setText(headingText);

                WebView content = root.findViewById(R.id.itemContentView);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(content.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                content.loadDataWithBaseURL(null, itemContent, "text/html; charset=utf-8", "utf8", null);


                Log.i ("tag", itemContent);

                FloatingActionButton fab = root.findViewById(R.id.editFab);

                fab.setOnClickListener(view -> {

                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                    String toolbarPref = pref.getString("pref_defaultToolbar", getString(R.string.pref_defaultToolbar_default));

                    if (toolbarPref.equals("single") || toolbarPref.equals("double")) {
                        //Toast.makeText(getContext(), R.string.loading_editor, Toast.LENGTH_LONG).show();
                    }

                    Fragment editFragment = new ItemEditFragment();
                    Bundle editBundle = new Bundle();
                    editBundle.putLong("studyId", studyId);
                    editBundle.putString("item", itemString);
                    editFragment.setArguments(editBundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left)
                            .replace(R.id.nav_host_fragment, editFragment)
                            .addToBackStack("tag")
                            .commit();
                });
            }
        }

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        Bundle bundle = this.getArguments();
        itemString = bundle.getString("item","");
        switch (itemString) {
            case "passage":
            case "observations":
            case "application":
            case "questions":
                inflater.inflate(R.menu.view, menu);
                break;
            case "key":
                inflater.inflate(R.menu.key, menu);
                break;
        }

        //super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack();
                break;
            case R.id.action_information_view:
            case R.id.action_information_key:
                Dialog informationDialog = new Dialog(getContext(), R.style.dialog);
                informationDialog.setContentView(R.layout.dialog_information);

                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.85);

                informationDialog.getWindow().setLayout(width, height);
                informationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                informationDialog.getWindow().setDimAmount(0.8f);
                informationDialog.setCanceledOnTouchOutside(true);

                WebView webView = informationDialog.findViewById(R.id.informationContent);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                Bundle bundle = this.getArguments();

                itemString = bundle.getString("item","");
                switch (itemString) {
                    case "passage":
                    case "observations":
                        webView.loadUrl("file:///android_asset/observation.html");
                        break;
                    case "questions":
                        webView.loadUrl("file:///android_asset/questions.html");
                        break;
                    case "key":
                        webView.loadUrl("file:///android_asset/key.html");
                        break;
                    case "application":
                        webView.loadUrl("file:///android_asset/application.html");
                        break;
                }

                webView.setBackgroundColor(Color.TRANSPARENT);

                FloatingActionButton closeFab = informationDialog.findViewById(R.id.closeFab);
                closeFab.setOnClickListener(view -> {
                    // fab.setVisibility(View.VISIBLE);
                    informationDialog.dismiss();
                });

                informationDialog.show();
                break;

            case R.id.action_tweet:
                List<Intent> targetShareIntents = new ArrayList<Intent>();
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");

                List<ResolveInfo> resInfos = getActivity().getPackageManager().queryIntentActivities(shareIntent, 0);
                if (!resInfos.isEmpty()) {
                    for (ResolveInfo resInfo : resInfos) {
                        //if (resInfo.activityInfo.name.equals("com.twitter.android.composer.ComposerActivity"))
                        if (resInfo.activityInfo.packageName.contains("twitter"))
                        {
                            Intent tweetIntent = new Intent();
                            tweetIntent.setAction(Intent.ACTION_SEND);
                            tweetIntent.setType("text/plain");
                            tweetIntent.putExtra(Intent.EXTRA_TEXT, passageHeading + " #IBSkey: " + Html.fromHtml(itemContent));
                            tweetIntent.setPackage(resInfo.activityInfo.packageName);
                            targetShareIntents.add(tweetIntent);
                        }
                    }

                }

                if (!targetShareIntents.isEmpty()) {
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), getResources().getString(R.string.tweet_share));
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                }

                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return super.onOptionsItemSelected(item);
    }

    public void goBack() {
        // make Bundle here
        Fragment studyFragment = new StudyFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("studyId", studyId);
        studyFragment.setArguments(bundle);

        // Handle the back button event
        getActivity().getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .setCustomAnimations(R.anim.pull_in_left, R.anim.push_out_right)
                .replace(R.id.nav_host_fragment, studyFragment)
                .addToBackStack("tag")
                .commit();
    }
}
