package mx.x10.ampers.inductivebiblestudy.ui.study;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import mx.x10.ampers.inductivebiblestudy.R;


/**
 * Created by VellaA on 18/12/2015.
 */
public class StudyItemArrayAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final long id;
    private final String[] texts;
    private final String[] images;
    private final String[] headings;

    public StudyItemArrayAdapter(Context context, String[] items, String[] images, long id ) {

        super(context, R.layout.row_study_item, items);
        this.context = context;
        this.texts = items;
        this.images = images;
        this.id = id;
        this.headings = new String[] {
                    "",
                    context.getResources().getString(R.string.observations_heading),
                    context.getResources().getString(R.string.questions_heading),
                    context.getResources().getString(R.string.key_heading),
                    context.getResources().getString(R.string.application_heading)
        };
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_study_item, parent, false);

        TextView heading = rowView.findViewById(R.id.studyItemHeading);
        TextView text = rowView.findViewById(R.id.itemText);
        ImageView image = rowView.findViewById(R.id.itemIcon);

        if (headings[position].isEmpty())
            heading.setVisibility(View.GONE);
        else
            heading.setText(headings[position]);

        if (texts[position] == null)
            text.setText(context.getResources().getString(R.string.item_empty));
        else
            text.setText(Html.fromHtml(texts[position]));

        image.setImageResource(context.getResources().getIdentifier(images[position], "drawable", context.getPackageName()));

        rowView.setTag(id);

        return rowView;
    }

}
