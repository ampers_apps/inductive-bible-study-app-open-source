package mx.x10.ampers.inductivebiblestudy.ui.item;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.onegravity.rteditor.RTEditText;
import com.onegravity.rteditor.RTManager;
import com.onegravity.rteditor.RTToolbar;
import com.onegravity.rteditor.api.RTApi;
import com.onegravity.rteditor.api.RTMediaFactoryImpl;
import com.onegravity.rteditor.api.RTProxyImpl;
import com.onegravity.rteditor.api.format.RTFormat;

import mx.x10.ampers.inductivebiblestudy.BibleStudyContract;
import mx.x10.ampers.inductivebiblestudy.BibleStudyDbHelper;
import mx.x10.ampers.inductivebiblestudy.MainActivity;
import mx.x10.ampers.inductivebiblestudy.R;
import mx.x10.ampers.inductivebiblestudy.ui.study.StudyFragment;

public class ItemEditFragment extends Fragment {

    long studyId;
    private RTManager mRTManager;
    private RTEditText mRTMessageField;
    String passageText;
    String passageTextHeading;
    String passageCopyright;
    String content;
    String passageHeading;
    String contentItem;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
        //pref.registerOnSharedPreferenceChangeListener(getContext());

        Bundle bundle = this.getArguments();

        studyId = bundle.getLong("studyId", -1);
        contentItem = bundle.getString("item","");
        String itemHeading = "";

        View root = inflater.inflate(R.layout.fragment_item_edit, container, false);

        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.item_edit_title);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(androidx.appcompat.R.drawable.abc_ic_ab_back_material);
        ((MainActivity)getActivity()).lockDrawer();

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                goBack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);


        if (studyId != -1) {

            // do sql based off study id and item
            BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getContext());
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            Cursor cursor = db.rawQuery("SELECT " + contentItem + ",heading,copyright,passage,heading FROM study WHERE id=" + studyId, null);

            if(cursor.moveToFirst()){
                content = cursor.getString(0);
                passageHeading = cursor.getString(1);
                passageCopyright= cursor.getString(2);
                passageText = cursor.getString(3);
                passageTextHeading = cursor.getString(4);
            }

            cursor.close();
            db.close();

            RTApi rtApi = new RTApi(getContext(), new RTProxyImpl(getActivity()), new RTMediaFactoryImpl(getContext(), true));
            mRTManager = new RTManager(rtApi, savedInstanceState);

            mRTMessageField = root.findViewById(R.id.rtEditText_1);
            mRTManager.registerEditor(mRTMessageField, true);
            mRTMessageField.setRichTextEditing(true, content);

            mRTMessageField.setMovementMethod(new ScrollingMovementMethod());
            mRTMessageField.setTextIsSelectable(true);

            mRTMessageField.requestFocus();
            mRTMessageField.setSelection(0);


            String toolbarPref = pref.getString("pref_defaultToolbar", getString(R.string.pref_defaultToolbar_default));

            FloatingActionButton fab = root.findViewById(R.id.viewFab);

            if (!toolbarPref.equals("none")) {
                RelativeLayout relativeEditLayout = root.findViewById(R.id.relativeToolbarLayout);

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                NestedScrollView scrollView = root.findViewById(R.id.nestedScrollLayout);
                ViewGroup.MarginLayoutParams scrollParams = (ViewGroup.MarginLayoutParams) scrollView.getLayoutParams();
                ViewGroup.MarginLayoutParams RTEditorPrams = (ViewGroup.MarginLayoutParams) mRTMessageField.getLayoutParams();

                CoordinatorLayout.LayoutParams fabParams = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();

                int sizeInDP = 40;

                RTToolbar rtToolbar0;

                mRTMessageField.setHint(R.string.empty_content);

                switch (toolbarPref) {
                    case "basic":
                        View basicToolbar = getLayoutInflater().inflate(R.layout.rte_toolbar_basic, null);
                        relativeEditLayout.addView(basicToolbar, layoutParams);

                        rtToolbar0 = root.findViewById(R.id.rte_toolbar_basic);
                        mRTManager.registerToolbar(relativeEditLayout, rtToolbar0);

                        break;

                    case "single":
                        View fullToolbar = getLayoutInflater().inflate(R.layout.rte_toolbar_full, null);
                        relativeEditLayout.addView(fullToolbar, layoutParams);

                        rtToolbar0 = root.findViewById(R.id.rte_toolbar_full);
                        mRTManager.registerToolbar(relativeEditLayout, rtToolbar0);

                        break;

                    case "double":
                        View doubleToolbar = getLayoutInflater().inflate(R.layout.rte_toolbar_double, null);
                        relativeEditLayout.addView(doubleToolbar, layoutParams);

                        rtToolbar0 = root.findViewById(R.id.rte_toolbar_character);
                        mRTManager.registerToolbar(relativeEditLayout, rtToolbar0);

                        RTToolbar rtToolbar1 = root.findViewById(R.id.rte_toolbar_paragraph);
                        mRTManager.registerToolbar(relativeEditLayout, rtToolbar1);

                        sizeInDP = 100;

                        break;
                }

                int marginInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, sizeInDP, getResources().getDisplayMetrics());

                scrollParams.bottomMargin = marginInDp;
                scrollView.setLayoutParams(scrollParams);

                RTEditorPrams.bottomMargin = marginInDp/2;
                mRTMessageField.setLayoutParams(RTEditorPrams);

                fabParams.bottomMargin = marginInDp;
                fab.setLayoutParams(fabParams);

            }

            switch(contentItem) {
                case "passage":
                    itemHeading = passageHeading;
                    TextView itemCopyright = root.findViewById(R.id.itemCopyright);
                    itemCopyright.setText(Html.fromHtml("<small>"+passageCopyright+"</small>"));
                    itemCopyright.setVisibility(View.VISIBLE);

                    /*
                    // deal with the keyboard poping up when editing the Bible text
                    mRTMessageField.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            v.onTouchEvent(event);   // handle the event first
                           // InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                           // if (imm != null) {
                           //     imm.hideSoftInputFromWindow(v.getWindowToken(), 0);  // hide the soft keyboard
                           // }
                            return true;
                        }
                    });*/

                    break;
                case "observations":
                    itemHeading = getString(R.string.observations_heading);
                    break;
                case "questions":
                    itemHeading = getString(R.string.questions_heading);
                    break;
                case "key":
                    itemHeading = getString(R.string.key_heading);
                    break;
                case "application":
                    itemHeading = getString(R.string.application_heading);
                    break;
            }

            TextView heading = root.findViewById(R.id.itemHeading);
            heading.setText(itemHeading);


            if (!itemHeading.equals(passageHeading)) {
                fab.setOnClickListener(view -> {
                    // displays the passage
                    final Dialog passageDialog = new Dialog(getContext(), R.style.dialog);
                    passageDialog.setContentView(R.layout.dialog_passage);

                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

                    passageDialog.getWindow().setLayout(width, height);
                    passageDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    passageDialog.getWindow().setDimAmount(0.5f);
                    passageDialog.setCanceledOnTouchOutside(true);

                    TextView dialogHeading = passageDialog.findViewById(R.id.dialogHeading);
                    dialogHeading.setText(Html.fromHtml(passageTextHeading));

                    WebView dialogPassage = passageDialog.findViewById(R.id.dialogContent);
                    dialogPassage.loadDataWithBaseURL(null, passageText, "text/html; charset=utf-8", "utf8", null);
                    //dialogPassage.loadData(passageText, "text/html; charset=UTF-8", null);
                    dialogPassage.setBackgroundColor(Color.TRANSPARENT);

                    TextView dialogCopyright = passageDialog.findViewById(R.id.dialogCopyright);
                    dialogCopyright.setText(Html.fromHtml(passageCopyright));

                    FloatingActionButton closeFab = passageDialog.findViewById(R.id.closefab);
                    closeFab.setOnClickListener(view1 -> {
                        fab.setVisibility(View.VISIBLE);
                        // toolbarContainer.setVisibility(View.VISIBLE);
                        passageDialog.dismiss();
                    });

                    passageDialog.setOnCancelListener(
                            dialog -> {
                                //When you touch outside of dialog bounds,
                                //the dialog gets canceled and this method executes.
                                fab.setVisibility(View.VISIBLE);
                                //   toolbarContainer.setVisibility(View.VISIBLE);
                            }
                    );

                    fab.setVisibility(View.INVISIBLE);
                    //  toolbarContainer.setVisibility(View.INVISIBLE);

                    passageDialog.show();
                });
            }else
            {
                CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                p.setBehavior(null);
                fab.setLayoutParams(p);
                fab.setVisibility(View.GONE);
            }

        }

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.edit, menu);
        //super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBack();
                break;

            case R.id.action_information_edit:
                Dialog informationDialog = new Dialog(getContext(), R.style.dialog);
                informationDialog.setContentView(R.layout.dialog_information);

                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.85);

                informationDialog.getWindow().setLayout(width, height);
                informationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                informationDialog.getWindow().setDimAmount(0.8f);
                informationDialog.setCanceledOnTouchOutside(true);

                WebView webView = informationDialog.findViewById(R.id.informationContent);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                Bundle bundle = this.getArguments();

                contentItem = bundle.getString("item","");
                switch (contentItem) {
                    case "passage":
                    case "observations":
                        webView.loadUrl("file:///android_asset/observation.html");
                        break;
                    case "questions":
                        webView.loadUrl("file:///android_asset/questions.html");
                        break;
                    case "key":
                        webView.loadUrl("file:///android_asset/key.html");
                        break;
                    case "application":
                        webView.loadUrl("file:///android_asset/application.html");
                        break;
                }

                webView.setBackgroundColor(Color.TRANSPARENT);

                FloatingActionButton closeFab = informationDialog.findViewById(R.id.closeFab);
                closeFab.setOnClickListener(view -> {
                    // fab.setVisibility(View.VISIBLE);
                    informationDialog.dismiss();
                });

                informationDialog.show();
                break;

            case R.id.action_save:
                saveContent();
                break;

            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause() {
        //save
        saveContent();

        super.onPause();
    }

    public void goBack() {
        //save
        if (!mRTMessageField.getText(RTFormat.HTML).isEmpty()) {
            saveContent();

            // make Bundle here
            Fragment viewFragment = new ItemViewFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("studyId", studyId);
            bundle.putString("item", contentItem);
            viewFragment.setArguments(bundle);

            // Handle the back button event
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .setCustomAnimations(R.anim.pull_in_left, R.anim.push_out_right)
                    .replace(R.id.nav_host_fragment, viewFragment)
                    .addToBackStack("tag")
                    .commit();
        }
        else
        {
            // no content so go to study fragment and not item fragment
            // make Bundle here
            Fragment studyFragment = new StudyFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("studyId", studyId);
            studyFragment.setArguments(bundle);

            // Handle the back button event
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .setCustomAnimations(R.anim.pull_in_left, R.anim.push_out_right)
                    .replace(R.id.nav_host_fragment, studyFragment)
                    .addToBackStack("tag")
                    .commit();
        }

        InputMethodManager imm = (InputMethodManager)getView().getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);  // hide the soft keyboard
        }
    }

    public void saveContent(){
        if (!mRTMessageField.getText(RTFormat.HTML).isEmpty()) {
            BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getContext());
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            ContentValues newValues = new ContentValues();
            newValues.put(contentItem, mRTMessageField.getText(RTFormat.HTML));
            newValues.put(BibleStudyContract.BibleStudyEntry.COLUMN_NAME_DATE_MODIFIED, System.currentTimeMillis());

            db.update("study", newValues, "id=" + studyId, null);
            db.close();

            Toast.makeText(getContext(), getString(R.string.save_alert), Toast.LENGTH_SHORT).show();
        }
    }

}
