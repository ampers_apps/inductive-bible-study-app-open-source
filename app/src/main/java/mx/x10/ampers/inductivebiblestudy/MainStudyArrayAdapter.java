package mx.x10.ampers.inductivebiblestudy;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

//import junit.framework.Assert;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrew on 17-Dec-15.
 */
public class MainStudyArrayAdapter extends ArrayAdapter<String> implements Filterable {

    private final Context context;
    private final String[] ids;
    private final String[] headings;
    private final String[] dates;

    private List<String>originalData = null;
    private List<String>filteredData = null;
    private ItemFilter mFilter = new ItemFilter();

    public MainStudyArrayAdapter(Context context, List<String> ids, List<String> headings, List<String> dates) {

        super(context, R.layout.row_study, headings);
        this.context = context;
        this.ids = ids.toArray(new String[ids.size()]);
        this.headings = headings.toArray(new String[headings.size()]);
        this.dates = dates.toArray(new String[dates.size()]);

        this.originalData = headings;
        this.filteredData  = headings;

    }

    public int getCount() {
        return filteredData.size();
    }
    public String getItem(int position) {
        return filteredData.get(position);
    }
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.row_study, parent, false);

        TextView textView = rowView.findViewById(R.id.passageHeading);
        ImageView imageView = rowView.findViewById(R.id.icon);
        TextView dateView = rowView.findViewById(R.id.passageDate);
        ImageButton deleteButton = rowView.findViewById(R.id.deleteButton);

        // This is all a bit dodge
        // This adapter started as just a filter for the heading, but then needs to really be an object
        // instead find the filtered heading in the heading array and get that id to populate the rest
        int index = -1;
        for (int i=0;i<headings.length;i++) {
            if (headings[i].equals(filteredData.get(position))) {
                index = i;
                break;
            }
        }

        rowView.setTag(ids[index]);
        textView.setText(filteredData.get(position));

        long dateLong = new Long(dates[index]);
        Date date = new Date(dateLong);
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy h:mm:ss a");

        dateView.setText(formatter.format(date));


        // Change the icon for bible book
        String text = filteredData.get(position);
        Pattern regex = Pattern.compile("([1|2|3]?([i|I]+)?(\\s?)\\w+(\\s+?))((\\d+)?(,?)(\\s?)(\\d+))+(:?)((\\d+)?([\\-–]\\d+)?(,(\\s?)\\d+[\\-–]\\d+)?)?");
        Matcher regexMatcher = regex.matcher(text);

        String imageName = "";

        if (regexMatcher.find()) {
            imageName = regexMatcher.group(1).toString();
            imageName = imageName.trim();
            imageName = imageName.replace(" ", "_");
            imageName = imageName.replace("1", "a");
            imageName = imageName.replace("2", "b");
            imageName = imageName.replace("3", "c");
            imageName = imageName.toLowerCase();
        }

        if (!imageName.isEmpty()) {
            Assert.assertNotNull(context);
            Assert.assertNotNull(imageName);
            imageView.setImageResource(context.getResources().getIdentifier(imageName, "drawable", context.getPackageName()));
        }

        deleteButton.setOnClickListener(v -> new MaterialAlertDialogBuilder(context,R.style.MaterialAlertDialog)
                .setMessage(R.string.delete_prompt)
                .setCancelable(false)
                .setPositiveButton(R.string.delete_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(context);
                        SQLiteDatabase db = mDbHelper.getReadableDatabase();
                        db.delete("study", "id = " + ids[position], null);

                        MainStudyArrayAdapter.this.remove(getItem(position));
                        MainStudyArrayAdapter.this.notifyDataSetChanged();

                        Activity activity = (Activity) context;

                        Intent intent = new Intent(rowView.getContext(), MainActivity.class);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


                    }
                })
                .setNegativeButton(R.string.delete_cancel, null)
                .show());

        return rowView;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<String> list = originalData;

            int count = list.size();
            final ArrayList<String> nlist = new ArrayList<String>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<String>) results.values;
            notifyDataSetChanged();
        }

    }
}
