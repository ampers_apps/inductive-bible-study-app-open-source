package mx.x10.ampers.inductivebiblestudy;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class InformationFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.activity_information, container, false);

        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.menu_info);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);
        ((MainActivity) getActivity()).lockDrawer();


        final WebView webView = root.findViewById(R.id.infoContent);

        int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

            if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
            }
        }

        webView.loadUrl("file:///android_asset/index.html");
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setWebViewClient(new MyWebViewClient());

        FloatingActionButton fab = root.findViewById(R.id.tocFab);
        fab.setOnClickListener(view1 -> webView.loadUrl("file:///android_asset/index.html"));


        return root;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
    }


    public class MyWebViewClient extends WebViewClient {
       /* @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.startsWith("file:///android_asset")) {

                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }*/

        public void onPageFinished(WebView view, String url) {
            view.scrollTo(0, 0);

            FloatingActionButton fab = getActivity().findViewById(R.id.tocFab);

            if (url.contains("index.html"))
                fab.setVisibility(View.INVISIBLE);
            else
                fab.setVisibility(View.VISIBLE);

        }

    }
}

