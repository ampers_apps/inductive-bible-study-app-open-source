package mx.x10.ampers.inductivebiblestudy;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class AboutFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.activity_about, container, false);

        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.menu_about);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);
        ((MainActivity)getActivity()).lockDrawer();

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                getActivity().moveTaskToBack(true);

            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        TextView textContact = root.findViewById(R.id.textContact);
        textContact.setText(Html.fromHtml(getString(R.string.contact_text)));
        textContact.setMovementMethod(LinkMovementMethod.getInstance());

        TextView textAbout = root.findViewById(R.id.textAbout);
        textAbout.setText(Html.fromHtml(getString(R.string.about_text)));
        textAbout.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
    }
}

