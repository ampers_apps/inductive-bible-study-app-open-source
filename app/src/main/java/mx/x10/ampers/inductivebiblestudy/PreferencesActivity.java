package mx.x10.ampers.inductivebiblestudy;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import mx.x10.ampers.inductivebiblestudy.ui.settings.PreferencesFragment;

/**
 * Created by VellaA on 23/12/2015.
 */
public class PreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(android.R.id.content, new PreferencesFragment())
                .commit();
    }

}

