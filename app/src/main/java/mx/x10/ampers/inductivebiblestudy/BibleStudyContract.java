package mx.x10.ampers.inductivebiblestudy;

import android.provider.BaseColumns;

/**
 * Created by VellaA on 17/12/2015.
 */
public final class BibleStudyContract {

    public static final String TABLE_NAME = "study";
    /**
     * Contains the SQL query to use to create the table containing the row counters.
     */
    public static final String SQL_CREATE_TABLE = "CREATE TABLE "
            + BibleStudyContract.TABLE_NAME + " ("
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_DATE_CREATED + " INTEGER,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_DATE_MODIFIED + " INTEGER,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_HEADING + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_PASSAGE + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_COPYRIGHT + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_OBSERVATIONS + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_QUESTIONS + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_KEY + " TEXT,"
            + BibleStudyContract.BibleStudyEntry.COLUMN_NAME_APPLICATION + " TEXT )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + BibleStudyContract.TABLE_NAME;


    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public BibleStudyContract() {}

    /* Inner class that defines the table contents */
    public static abstract class BibleStudyEntry implements BaseColumns {

        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_DATE_CREATED = "dateCreated";
        public static final String COLUMN_NAME_DATE_MODIFIED = "dateModified";
        public static final String COLUMN_NAME_HEADING = "heading";
        public static final String COLUMN_NAME_PASSAGE = "passage";
        public static final String COLUMN_NAME_COPYRIGHT = "copyright";
        public static final String COLUMN_NAME_OBSERVATIONS = "observations";
        public static final String COLUMN_NAME_QUESTIONS = "questions";
        public static final String COLUMN_NAME_KEY = "key";
        public static final String COLUMN_NAME_APPLICATION = "application";

    }

}
