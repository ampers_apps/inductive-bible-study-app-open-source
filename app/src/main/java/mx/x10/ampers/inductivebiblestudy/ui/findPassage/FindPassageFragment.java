package mx.x10.ampers.inductivebiblestudy.ui.findPassage;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mx.x10.ampers.inductivebiblestudy.BibleStudyContract;
import mx.x10.ampers.inductivebiblestudy.BibleStudyDbHelper;
import mx.x10.ampers.inductivebiblestudy.MainActivity;
import mx.x10.ampers.inductivebiblestudy.R;
import mx.x10.ampers.inductivebiblestudy.ui.home.HomeFragment;
import mx.x10.ampers.inductivebiblestudy.ui.study.StudyFragment;

public class FindPassageFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.fragment_find_passage, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.find_passage_title);


        // check if coming from Australian Daily Prayer app
        Intent intent = getActivity().getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent, root); // Handle text being sent
            }
        } else  {

            final EditText passageString = root.findViewById(R.id.passageString);
            passageString.requestFocus();
        }

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                getActivity().getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .replace(R.id.nav_host_fragment, new HomeFragment())
                        .commit();

            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        SharedPreferences pref = android.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
        //pref.registerOnSharedPreferenceChangeListener(getActivity());
        Boolean bibleHeadings = pref.getBoolean("pref_hideHeadings", true);

        final String rootURL = "https://api.esv.org/v3/passage/html/?q=";
        String endURL ="include-passage-references=false&include-footnotes=false&include-audio-link=false&include-passage-references=false&include-short-copyright=false";

        // check preference for titles to be included or not
        if (bibleHeadings == false) {
            endURL = endURL + "&include-headings=false";
        }

        Button searchButton = root.findViewById(R.id.searchButton);

        String finalEndURL = endURL;
        searchButton.setOnClickListener(view -> {
            TextView passageString = root.findViewById(R.id.passageString);

            getBiblePassageTask getPassageData = new getBiblePassageTask(result -> displayJSONcontent(result, root), getActivity());

            getPassageData.execute(rootURL + Uri.encode(passageString.getText().toString()) + finalEndURL);

        });

        Button createButton = root.findViewById(R.id.createButton);
        createButton.setOnClickListener(view -> {
            TextView passageHeading = root.findViewById(R.id.passageHeading);
            TextView passageResult = root.findViewById(R.id.passageResult);
            TextView passageCopyright = root.findViewById(R.id.passageCopyright);

            BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getContext());
            SQLiteDatabase db = mDbHelper.getWritableDatabase();

            String studyHeading = passageHeading.getText().toString();
            String studyPassage = Html.toHtml(new SpannableString(passageResult.getText()));
            String studyCopyright = Html.toHtml(new SpannableString(passageCopyright.getText()));

            ContentValues studyValues = new ContentValues();
            studyValues.put (BibleStudyContract.BibleStudyEntry.COLUMN_NAME_DATE_CREATED, System.currentTimeMillis());
            studyValues.put (BibleStudyContract.BibleStudyEntry.COLUMN_NAME_DATE_MODIFIED, System.currentTimeMillis());
            studyValues.put (BibleStudyContract.BibleStudyEntry.COLUMN_NAME_HEADING, studyHeading);
            studyValues.put (BibleStudyContract.BibleStudyEntry.COLUMN_NAME_PASSAGE, studyPassage);
            studyValues.put (BibleStudyContract.BibleStudyEntry.COLUMN_NAME_COPYRIGHT, studyCopyright);

            long newRowId;
            newRowId = db.insert(BibleStudyContract.TABLE_NAME, null, studyValues);

            Fragment studyFragment = new StudyFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("studyId", newRowId);
            studyFragment.setArguments(bundle);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left)
                    .replace(R.id.nav_host_fragment, studyFragment)
                    .addToBackStack("tag")
                    .commit();

        });

        return root;
    }

    void handleSendText(Intent intent, View root) {
        String sharedText = intent.getStringExtra("sendingApp");

        // only work if we are coming from Daily Prayer app
        if (sharedText.equals("mx.x10.ampers.dailyprayer"))
        {
            String headingText = intent.getStringExtra("passageHeading");
            String passageText = intent.getStringExtra("passageText");
            String copyrightText = intent.getStringExtra("copyrightText");

            TextView passageHeading = root.findViewById(R.id.passageHeading);
            TextView passageResult = root.findViewById(R.id.passageResult);
            TextView passageCopyright = root.findViewById(R.id.passageCopyright);

            passageHeading.setText(headingText);
            passageResult.setText(Html.fromHtml(passageText));
            passageCopyright.setText(Html.fromHtml("<small>" + copyrightText + "</small>"));

            Button createButton = root.findViewById(R.id.createButton);
            createButton.setEnabled(true);
        }
    }

    private void displayJSONcontent(String result, View root) {

        if(result != null) {
            try {
                // https://api.esv.org/docs/passage-html/
                JSONArray htmlPassage =new JSONObject(result).getJSONArray("passages");
                String passageTitle =new JSONObject(result).getString("canonical");

                //Log.i(TAG, "text is: " + new JSONObject(result).toString());

                TextView passageHeading = root.findViewById(R.id.passageHeading);
                passageHeading.setText(passageTitle.toString());

                TextView passageResult = root.findViewById(R.id.passageResult);
                passageResult.setText(Html.fromHtml(htmlPassage.getString(0).replace("&#160;", " ")));

                TextView passageCopyright = root.findViewById(R.id.passageCopyright);
                passageCopyright.setText(getString(R.string.ESVcopyright));

                Button createStudy = root.findViewById(R.id.createButton);
                createStudy.setEnabled(true);

            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public interface FragmentCallback {
        public void onTaskDone(String result);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.find, menu);
        //super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_information_find:
                Dialog informationDialog = new Dialog(getContext(), R.style.dialog);
                informationDialog.setContentView(R.layout.dialog_information);

                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.85);

                informationDialog.getWindow().setLayout(width, height);
                informationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                informationDialog.getWindow().setDimAmount(0.8f);
                informationDialog.setCanceledOnTouchOutside(true);

                WebView webView = informationDialog.findViewById(R.id.informationContent);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                webView.loadUrl("file:///android_asset/passage.html");
                webView.setBackgroundColor(Color.TRANSPARENT);

                FloatingActionButton closeFab = informationDialog.findViewById(R.id.closeFab);
                closeFab.setOnClickListener(view -> {
                    // fab.setVisibility(View.VISIBLE);
                    informationDialog.dismiss();
                });

                informationDialog.show();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
