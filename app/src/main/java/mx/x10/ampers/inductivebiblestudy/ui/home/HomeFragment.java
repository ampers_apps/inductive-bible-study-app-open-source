package mx.x10.ampers.inductivebiblestudy.ui.home;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import mx.x10.ampers.inductivebiblestudy.BibleStudyDbHelper;
import mx.x10.ampers.inductivebiblestudy.MainActivity;
import mx.x10.ampers.inductivebiblestudy.MainStudyArrayAdapter;
import mx.x10.ampers.inductivebiblestudy.PreferencesActivity;
import mx.x10.ampers.inductivebiblestudy.R;
import mx.x10.ampers.inductivebiblestudy.ui.findPassage.FindPassageFragment;
import mx.x10.ampers.inductivebiblestudy.ui.study.StudyFragment;

public class HomeFragment extends Fragment {

    MainStudyArrayAdapter adapter;
    ListView mListView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle(R.string.app_name);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.baseline_menu_white_18dp);
        ((MainActivity)getActivity()).lockDrawer();

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                getActivity().moveTaskToBack(true);

            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getActivity());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT id,heading,dateModified FROM study ORDER BY dateModified DESC", null);

        List<String> ids = new ArrayList<String>();
        List<String> headings = new ArrayList<String>();
        List<String> dates = new ArrayList<String>();

        if(cursor.moveToFirst()){
            do{
                //assigning values
                ids.add(cursor.getString(0));
                headings.add(cursor.getString(1));
                dates.add(cursor.getString(2));

            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        if (headings.size() == 0)
        {
            TextView emptyText = root.findViewById(R.id.emptyMessage);
            emptyText.setVisibility(View.VISIBLE);
        }
        else {
            mListView = root.findViewById(R.id.studyList);

            adapter = new MainStudyArrayAdapter(getActivity(), ids, headings, dates);
            mListView.setAdapter(adapter);

            mListView.setOnItemClickListener((parent, view, position, id) -> {
                String studyId = (String) view.getTag();

                Fragment studyFragment = new StudyFragment();
                Bundle bundle = new Bundle();
                bundle.putLong("studyId", Long.parseLong(studyId));
                studyFragment.setArguments(bundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .setReorderingAllowed(true)
                        .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left)
                        .replace(R.id.nav_host_fragment, studyFragment)
                        .commit();

            });

            setListViewHeightBasedOnChildren(mListView);
        }

        FloatingActionButton fab = root.findViewById(R.id.createFab);
        fab.setOnClickListener(view -> getActivity().getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .addToBackStack(null)
                .replace(R.id.nav_host_fragment, new FindPassageFragment())
                .commit());

        return root;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        //int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            //listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        //super.onCreateOptionsMenu(menu,inflater);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(getContext().SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setImeOptions(searchView.getImeOptions() | EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_FLAG_NO_FULLSCREEN);
        //searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextChange(String newText)
            {
                // this is your adapter that will be filtered
                adapter.getFilter().filter(newText);
                //System.out.println("on text chnge text: "+newText);
                setListViewHeightBasedOnChildren(mListView);
                return true;
            }
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                // this is your adapter that will be filtered
                adapter.getFilter().filter(query);
                //System.out.println("on query submit: "+query);
                setListViewHeightBasedOnChildren(mListView);
                return true;
            }

        };

        searchView.setOnCloseListener(() -> {
            MenuItem menuSearch = menu.findItem(R.id.action_search);
            searchView.setQuery("", false);
            setListViewHeightBasedOnChildren(mListView);
            searchView.onActionViewCollapsed();
            return false;
        });

        searchView.setOnQueryTextListener(textChangeListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_information_home:
                Dialog informationDialog = new Dialog(getContext(), R.style.dialog);
                informationDialog.setContentView(R.layout.dialog_information);

                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.85);

                informationDialog.getWindow().setLayout(width, height);
                informationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                informationDialog.getWindow().setDimAmount(0.8f);
                informationDialog.setCanceledOnTouchOutside(true);

                WebView webView = (WebView) informationDialog.findViewById(R.id.informationContent);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                webView.loadUrl("file:///android_asset/overview.html");
                webView.setBackgroundColor(Color.TRANSPARENT);

                FloatingActionButton closeFab = informationDialog.findViewById(R.id.closeFab);
                closeFab.setOnClickListener(view -> {
                   // fab.setVisibility(View.VISIBLE);
                    informationDialog.dismiss();
                });

                informationDialog.show();
                break;

            case R.id.action_settings:


                Intent settingsIntent = new Intent(getActivity().getApplicationContext(), PreferencesActivity.class);
                //settingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(settingsIntent);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

}