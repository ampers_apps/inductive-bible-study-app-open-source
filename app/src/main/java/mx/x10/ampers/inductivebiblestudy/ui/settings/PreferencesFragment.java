package mx.x10.ampers.inductivebiblestudy.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;

import mx.x10.ampers.inductivebiblestudy.R;

import static android.content.Context.MODE_PRIVATE;

public class PreferencesFragment extends PreferenceFragmentCompat {

    public PreferencesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState,
                                    String rootKey) {

        setPreferencesFromResource(R.xml.preferences, rootKey);

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(android.R.color.white));

        return view;

    }


    @Override
    public void onStop() {
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        CheckBoxPreference pref_hideHeadings = findPreference("pref_hideHeadings");
        editor.putBoolean("pref_hideHeadings",pref_hideHeadings.isChecked());

        ListPreference pref_defaultToolbar = findPreference("pref_defaultToolbar");
        editor.putString("pref_defaultToolbar", pref_defaultToolbar.getValue());


        // Commit the edits!
        editor.commit();
        //getActivity().finish();
        //Intent mainIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        //mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivity(mainIntent);
    }


}