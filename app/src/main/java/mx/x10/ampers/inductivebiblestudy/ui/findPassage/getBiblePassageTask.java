package mx.x10.ampers.inductivebiblestudy.ui.findPassage;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Andrew on 16-Dec-15.
 */
public class getBiblePassageTask extends AsyncTask<String, String, String> {

    HttpURLConnection urlConnection;
    private Context mContext;
    private FindPassageFragment.FragmentCallback mFragmentCallback;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;

    public getBiblePassageTask(FindPassageFragment.FragmentCallback fragmentCallback, Context context)
    {
        mFragmentCallback = fragmentCallback;
        mContext = context;
    }

    @Override
    protected String doInBackground(String... uri) {

        StringBuilder result = new StringBuilder();

        try {
            // ESV API
            URL url = new URL(uri[0]);
            urlConnection = (HttpURLConnection) url.openConnection();

            String apikey = "<insert ESV API key here>";
            urlConnection.setRequestMethod("GET");
            urlConnection.addRequestProperty("Authorization", "Token " + apikey);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

        } catch (Exception e) {

            e.printStackTrace();

        } finally {
            urlConnection.disconnect();
        }

        return result.toString();
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading, please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (!result.isEmpty())
            mFragmentCallback.onTaskDone(result);
        else {
            alertDialog = new AlertDialog.Builder(mContext).create();
            alertDialog.setMessage("Error, no data... Please check your Internet connection.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> {
                        //  Intent A = new Intent(mContext, NoConnectionActivity.class);
                        //  mContext.startActivity(A);
                        alertDialog.dismiss();
                    });
            alertDialog.setOnDismissListener(dialog -> {
                // Intent A = new Intent(mContext, NoConnectionActivity.class);
                // mContext.startActivity(A);
                alertDialog.dismiss();
            });
            alertDialog.show();
            super.onPreExecute();
        }
    }

}