package mx.x10.ampers.inductivebiblestudy.ui.study;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.webkit.WebSettingsCompat;
import androidx.webkit.WebViewFeature;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.x10.ampers.inductivebiblestudy.BibleStudyDbHelper;
import mx.x10.ampers.inductivebiblestudy.MainActivity;
import mx.x10.ampers.inductivebiblestudy.R;
import mx.x10.ampers.inductivebiblestudy.ui.home.HomeFragment;
import mx.x10.ampers.inductivebiblestudy.ui.item.ItemViewFragment;

public class StudyFragment extends Fragment {

    String studyHeading;
    String dateModified;
    String dateCreated;
    String copyright;
    String[] items = new String [5];

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        View root = inflater.inflate(R.layout.fragment_study, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.study_title);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(androidx.appcompat.R.drawable.abc_ic_ab_back_material);
        ((MainActivity)getActivity()).lockDrawer();

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                goBack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            long studyId = bundle.getLong("studyId", -1);

            // get the values from the db and populate
            BibleStudyDbHelper mDbHelper = new BibleStudyDbHelper(getContext());
            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            Cursor cursor = db.rawQuery("SELECT dateCreated, dateModified, heading, copyright, passage, observations, questions, key, application FROM study WHERE id = '" + studyId + "'", null);

            if(cursor.moveToFirst()){
                dateCreated = cursor.getString(0);
                dateModified = cursor.getString(1);
                studyHeading = cursor.getString(2);
                copyright = cursor.getString(3);

                items[0] = cursor.getString(4); // passage
                items[1] = cursor.getString(5); // observations
                items[2] = cursor.getString(6); // questions
                items[3] = cursor.getString(7); // key
                items[4] = cursor.getString(8); // application

                // Change the icon for bible book
                String text = studyHeading;
                Pattern regex = Pattern.compile("([1|2|3]?([i|I]+)?(\\s?)\\w+(\\s+?))((\\d+)?(,?)(\\s?)(\\d+))+(:?)((\\d+)?([\\-–]\\d+)?(,(\\s?)\\d+[\\-–]\\d+)?)?");
                Matcher regexMatcher = regex.matcher(text);
                String imageName = "";

                if (regexMatcher.find()) {
                    imageName = regexMatcher.group(1).toString();
                    imageName = imageName.trim();
                    imageName = imageName.replace(" ", "_");
                    imageName = imageName.replace("1", "a");
                    imageName = imageName.replace("2", "b");
                    imageName = imageName.replace("3", "c");
                    imageName = imageName.toLowerCase();
                }

                String[] images = {
                        imageName,
                        "light",
                        "question",
                        "key",
                        "application"
                };

                TextView studyHeadingTV = root.findViewById(R.id.passageHeading);
                studyHeadingTV.setText(studyHeading);

                ListView mListView = root.findViewById(R.id.studyItemList);

                StudyItemArrayAdapter adapter = new StudyItemArrayAdapter(getContext(), items, images, studyId);
                mListView.setAdapter(adapter);

                mListView.setOnItemClickListener((AdapterView.OnItemClickListener) (parent, view, position, id) -> {

                    TextView heading = view.findViewById(R.id.studyItemHeading);
                    String item = heading.getText().toString();

                    String[] itemName = item.split(" "); // get the first word in heading as a flag for what is being click on

                    long studyId1 = (long) view.getTag();

                    if (itemName[0] == "")
                        item = "passage";
                    else
                        item = itemName[0].toLowerCase();

                    // make Bundle here
                    Fragment viewFragment = new ItemViewFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putLong("studyId", studyId1);
                    bundle1.putString("item", item);
                    viewFragment.setArguments(bundle1);


                    getActivity().getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left)
                            .replace(R.id.nav_host_fragment, viewFragment)
                            .addToBackStack("tag")
                            .commit();

                });

                //setListViewHeightBasedOnChildren(mListView);

            }
            cursor.close();
            db.close();

            FloatingActionButton fab = root.findViewById(R.id.shareFab);

            final int currentapiVersion = android.os.Build.VERSION.SDK_INT;


            if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        long dateLong = new Long(dateModified);
                        Date date = new Date(dateLong);
                        SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy h:mm:ss a");

                        String shareString =
                                "<small><p style='text-align:right'>Last modified: " + formatter.format(date).toString() + "</p></small>" +
                                        "<h1>" + studyHeading + "</h1>" +
                                        checkNull(items[0]) + "<small>" + copyright + "</small>" +
                                        "<h2>" + getString(R.string.observations_heading) + "</h2><table><tr><td valign='top'><img src='file:///android_res/drawable/light.png' width='100px' height='100px' align='left' style='margin-right:20px;'></td><td>" + checkNull(items[1]) + "</td></tr></table>" +
                                        "<h2>" + getString(R.string.questions_heading) + "</h2><table><tr><td valign='top'><img src='file:///android_res/drawable/question.png' width='100px' height='100px' align='left' style='margin-right:20px'>" + checkNull(items[2]) + "</td></tr></table>" +
                                        "<h2>" + getString(R.string.key_heading) + "</h2><table><tr><td valign='top'><img src='file:///android_res/drawable/key.png' width='100px' height='100px' align='left' style='margin-right:20px'>" + checkNull(items[3]) + "</td></tr></table>" +
                                        "<h2>" + getString(R.string.application_heading) + "</h2><table><tr><td valign='top'><img src='file:///android_res/drawable/application.png' width='100px' height='100px' align='left' style='margin-right:20px'>" + checkNull(items[4]) + "</td></tr></table>" +
                                        "<center><br><hr width='80%'><small><i>" + getString(R.string.email_footer) + "</i></small></center>";

                        WebView webView = new WebView(getContext());

                        int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                        if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                            if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                                WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                            }
                        }

                        webView.setWebViewClient(new WebViewClient() {

                            public boolean shouldOverrideUrlLoading(WebView view, String url)
                            {
                                return false;
                            }

                            @Override
                            public void onPageFinished(WebView view, String url)
                            {
                                String title = "Inductive Bible Study: " + studyHeading + ".pdf";
                                title = title.replace(":", "_");

                                PrintManager printManager = (PrintManager) getActivity().getSystemService(Context.PRINT_SERVICE);
                                PrintDocumentAdapter printAdapter = view.createPrintDocumentAdapter(title);
                                printManager.print(title, printAdapter, new PrintAttributes.Builder().build());

                            }
                        });

                        webView.loadDataWithBaseURL(null, shareString, "text/HTML", "UTF-8", null);
                    }
                });
            }
            else
            {
                CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                p.setBehavior(null);
                fab.setLayoutParams(p);
                fab.setVisibility(View.GONE);
            }


        }
        else
        {
            // something went wrong... back to home
            goBack();
        }

        return root;
    }


    public String checkNull (String string){
        if (string == null)
            return "";
        else
            return string;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.study, menu);
        //super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                goBack();
                break;

            case R.id.action_information_study:
                Dialog informationDialog = new Dialog(getContext(), R.style.dialog);
                informationDialog.setContentView(R.layout.dialog_information);

                int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
                int height = (int)(getResources().getDisplayMetrics().heightPixels*0.85);

                informationDialog.getWindow().setLayout(width, height);
                informationDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                informationDialog.getWindow().setDimAmount(0.8f);
                informationDialog.setCanceledOnTouchOutside(true);

                WebView webView = informationDialog.findViewById(R.id.informationContent);

                int nightModeFlags = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
                if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {

                    if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
                        WebSettingsCompat.setForceDark(webView.getSettings(), WebSettingsCompat.FORCE_DARK_ON);
                    }
                }

                webView.loadUrl("file:///android_asset/study.html");
                webView.setBackgroundColor(Color.TRANSPARENT);

                FloatingActionButton closeFab = informationDialog.findViewById(R.id.closeFab);
                closeFab.setOnClickListener(view -> {
                    // fab.setVisibility(View.VISIBLE);
                    informationDialog.dismiss();
                });

                informationDialog.show();

            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    public void goBack() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .setCustomAnimations(R.anim.pull_in_left, R.anim.push_out_right)
                .replace(R.id.nav_host_fragment, new HomeFragment())
                .commit();
    }


}
