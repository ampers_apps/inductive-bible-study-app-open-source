Inductive Bible Study
This repo contains all the code for the Inductive Bible Study Android app, minus the API key for ESV.org. For this to work you must register your own API key at https://api.esv.org/ and set authString in the file getBiblePassageTask.java

This app uses following:

Android RTEditor by Emanuel Moecklin: https://github.com/1gravity/Android-RTEditor
Bible icons: overviewbible.com/free-bible-icons/
Bibles from: https://api.esv.org/
Android RTEditor is Copyright 2015 Emanuel Moecklin licensed under the Apache License, Version 2.0 (the "License"). Likewise this app is also released under this licence.

You may not use this file except in compliance with the License. You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.